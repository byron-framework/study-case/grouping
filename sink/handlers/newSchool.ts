const newSchool = async (ctx: any, msg: any): Promise<void> => {
  const data: any = JSON.parse(msg.getData());
  await ctx.db.School.create({ ...data });
};

export default newSchool;
