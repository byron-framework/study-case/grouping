const newClassroom = async (ctx: any, msg: any) => {
  const { db: { Classroom } }: any = ctx;
  const data: any = JSON.parse(msg.getData());
  const { _id, year, schoolId }: any = data;
  await Classroom.create({
    _id,
    year,
    schoolId,
  });
}

export default newClassroom;
