const newUserRole = async (ctx: any, msg: any) => {
  const { db: { User } }: any = ctx;

  const data: any = JSON.parse(msg.getData());
  const user: any = await User.findById(data.id);

  if (data.role === 'teacher' && user.role === 'coord'){
    return;
  }

  user.role = data.role;

  await user.save();
}

export default newUserRole;
