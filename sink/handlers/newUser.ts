const newUser = async (ctx: any, msg: any): Promise<void> => {
  const data: any = JSON.parse(msg.getData());
  await ctx.db.User.create({ _id: data._id, role: 'user' });
};

export default newUser;
