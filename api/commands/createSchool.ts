const createSchool = async (_p: any, args: any, ctx: any, _i: any): Promise<any> => {
  const { db: { School, User }, Error, uuidv4, Event }: any = ctx;
  const { name, address }: any = args.data;

  let school = await School.findOne({ name, address });

  if (school) {
    throw new Error('school already exists', '409');
  }

  let user = await User.findById(args.userId);

  if (!user) {
    throw new Error('User doesn\'t already exists', '409');
  }

  school = {
    _id: uuidv4(),
    name,
    address,
  };

  Event.emit('new.school', { ...school, user: user.id });
  Event.emit('new.user.role', { 
    id: args.userId,
    role: 'coord',
    schoolId: school._id,
  });

  return { ...school, user };
};

export default createSchool;
