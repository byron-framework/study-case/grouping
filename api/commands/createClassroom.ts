const createClassrom = async (_p: any, args: any, ctx: any, _i: any) => {
  const { db: { Classroom, User }, Error, uuidv4, Event }: any = ctx;
  const { year, schoolId }: any = args.data;

  let classroom = await Classroom.findOne({ year, schoolId });

  if (classroom) {
    throw new Error('Classroom already created', '409');
  }

  let user = await User.findById(args.userId);

  if (!user) {
    throw new Error('User doesn\'t already exists', '409');
  }

  classroom = {
    _id: uuidv4(),
    year,
    schoolId,
  };

  Event.emit('new.classroom', classroom);
  Event.emit('new.user.role', {
    id: args.userId,
    role: 'teacher'
  });

  return classroom;
}

export default createClassrom;
