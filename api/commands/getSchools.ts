const getSchools = async (_: any, __: any, ctx: any, ___: any): Promise<any[]> => {
  const { db: { School } }: any = ctx;
  const school: any = await School
    .find()
    .populate({
      path: 'user',
    });
  console.log(school);

  return school;
};

export default getSchools;
